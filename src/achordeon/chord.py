from .data import Note, semitones as semi
import re


class Chord:
    """
    Class for parsing and working with chords.

    :param chord_input: a string to be parsed as a chord
    """
    def __init__(self, chord_input: str):
        self.chord_input = chord_input
        self.root = None
        self.slash = None
        self.semitones = set()
        self.notes = set()

        self.symbols = None
        self.symbol_dict = None
        self.parse(chord_input)

    def __str__(self) -> str:
        return "".join(filter(None, self.symbols))

    def __len__(self) -> int:
        return 1 + len(self.semitones)  # root/slash + other notes

    def parse(self, chord_input: str):
        """
        Method for splitting chord input into symbols.
        Symbols are being parsed using regular expressions.

        root:
            Root note of the chord, required symbol. Sharps are denoted by *#*.
        slash:
            A note replacing the root in the first position separated from the root by a */*.
            Optional symbol
        triad:
            A combination of thirds and fifths completing the base of
            the chord with major triad being the default.
        seventh:
            An addition of a seventh to the chord.
            Optional symbol.
        ninth:
            An addition of a ninth to the chord.
            If there is no seventh specified, a minor seventh is added.
            Optional symbol.
        add:
            An addition of a specified interval. Sharps are denoted by *s*.
        sus:
            Suspending the third for a second or a fourth.
            Optional symbol.

        :param chord_input: a string to be parsed as a chord
        """
        # how much syntactically incorrect things do I want to parse?
        root = "(?P<root>^[A-G]#?b?)"
        slash = "(?:(/)(?P<slash>[A-G]#?b?))?"
        triad = "(?P<triad>m(?!aj)|dim|\+)?"
        seventh = "(?P<seventh>7|maj)?"
        ninth = "(?P<ninth>#?b?9)?"
        add = "(?:(add)(?P<add>#?b?\d+))?"
        sus = "(?:(sus)(?P<sus>[24]))?"

        chord_pattern = re.compile(root + slash + triad + seventh + ninth + add + sus)
        chord_input = re.search(chord_pattern, chord_input)

        if chord_input is None:
            raise ValueError(f"Invalid chord {self.chord_input}")

        self.symbols = chord_input.groups()
        self.symbol_dict = chord_input.groupdict()
        self.symbols_to_notes()

    def symbols_to_notes(self):
        """
        Method for turning parsed symbols into semitones which are them translated to notes.

        The semitones are relative to the root note of the chord.
        """
        root = self.symbol_dict.get("root")
        slash = self.symbol_dict.get("slash")
        triad = self.symbol_dict.get("triad")
        seventh = self.symbol_dict.get("seventh")
        ninth = self.symbol_dict.get("ninth")
        add = self.symbol_dict.get("add")
        sus = self.symbol_dict.get("sus")

        self.root = Note.val_by_name(root)
        if slash is not None:
            self.slash = Note.val_by_name(slash)

        # triad
        if triad is None:  # major
            self.semitones.add(semi("M3"))
            self.semitones.add(semi("P5"))
        elif triad == "m":  # minor
            self.semitones.add(semi("m3"))
            self.semitones.add(semi("P5"))
        elif triad == "dim":  # diminished
            self.semitones.add(semi("m3"))
            self.semitones.add(semi("d5"))
        else:  # augmented
            self.semitones.add(semi("M3"))
            self.semitones.add(semi("A5"))

        # 7th
        if seventh is not None or ninth is not None:
            if seventh == "maj":
                if triad == "dim":
                    self.semitones.add(semi("m7"))
                else:
                    self.semitones.add(semi("M7"))
            else:
                if triad == "dim":
                    self.semitones.add(semi("d7"))
                else:
                    self.semitones.add(semi("m7"))

        # 9th
        if ninth is not None:
            if ninth == "9":
                self.semitones.add(semi("M9"))
            elif ninth[0] == "b":
                self.semitones.add(semi("m9"))
            else:
                self.semitones.add(semi("A9"))

        # add
        if add is not None:
            self.semitones.add(semi(add))

        # sus
        if sus is not None:
            if semi("M3") in self.semitones:
                self.semitones.remove(semi("M3"))
            else:
                self.semitones.remove(semi("m3"))
            if sus == "2":
                self.semitones.add(semi("M2"))
            else:
                self.semitones.add(semi("P4"))

        self.get_base_notes()

    def get_base_notes(self):
        """
        A function for getting notes out of semitones.

        The semitones are relative to the root note of the chord.
        """
        self.notes.add(self.root)
        for semitone in self.semitones:
            note = (self.root + semitone) % 12
            self.notes.add(note)
