from .chord import Chord
from .guitar import Fingering as Fing
import argparse

# ANSI
RED = "\033[91m"
RESET = "\033[0m"


def main():
    """
    The main entry point of the program.

    This function handles parsing command-line arguments and delegates tasks for
    chord parsing and construction of suitable guitar fingerings.
    """
    parser = argparse.ArgumentParser(
        prog="Achordeon",
        description="Achordeon Chord Fingering Generator",
        epilog="For more information on chord syntax, see the project README."
    )
    parser.add_argument("chord", help="symbolic description of a chord")
    parser.add_argument("-s", "--strings", metavar="N", type=str, nargs="*", help="a list of string notes")
    parser.add_argument("-l", "--length", type=int, help="neck length")
    parser.add_argument("-a", "--avg", type=int, help="average fingering span")
    parser.add_argument("-M", "--Muted", action="store_false", help="ignore muted strings when evaluating")
    parser.add_argument("-n", "--number", type=int, help="number of suggestions shown")
    args = parser.parse_args()

    try:
        chord = Chord(args.chord)
        kwargs = {
            "chord": chord,
            "strings": None if args.strings is None else args.strings[::-1],
            "neck_length": args.length,
            "avg_span": args.avg,
            "eval_muted": args.Muted,
            "sugg_num": args.number
        }
        fingering = Fing(**{k: v for k, v in kwargs.items() if v is not None})
    except ValueError as e:
        print(f"{RED}Error: {e}{RESET}")
        exit(1)
    print(chord)
    print(fingering)


if __name__ == '__main__':
    main()
