from .chord import Chord
from .data import Note
from collections import defaultdict
import itertools


def barre_req(option: tuple) -> int:
    """
    A function for calculating the required number of fingers used for barré chords.

    :param option: a tuple with fret positions in a fingering for each string
    :return: required number of fingers
    """
    req = len([i for i in option if i > 0])
    min_pos = min(filter(lambda i: i > 0, option), default=0)

    if 0 not in option:
        for string_pos in option:
            if string_pos == min_pos:
                req -= 1
    return req + 1  # take the finger making the barré into consideration


def string_positions(length: int, string_note: int, note: int) -> list:
    """
    A function for finding all possible fret positions for playing a note on a string.
    :param length: total number of fret positions
    :param string_note: base note of the string
    :param note: desired note
    :return: possible fret positions
    """
    positions = []
    for pos in range(length):
        if (string_note + pos) % 12 == note:
            positions.append(pos)

    return positions


class Fingering:
    """
    A class for finding fingerings for the chord specified.

    :param chord: chord object
    :param strings: list of string notes stored in reversed order for better code manipulation
    :param neck_length: neck length of the guitar used
    :param avg_span: average span over frets in a guitar fingering
    :param eval_muted: whether muted strings in the middle of the stroke
        should be considered during chord evaluation
    :param sugg_num: how many suggestions should be returned when printing
    """
    def __init__(self, chord: Chord, strings=None, neck_length=14, avg_span=2, eval_muted=True, sugg_num=3):
        if strings is None:
            strings = ["E", "A", "D", "G", "B", "E"]
        self.strings = [Note.val_by_name(note) for note in strings]
        self.neck_length = neck_length
        self.avg_span = avg_span
        self.eval_muted = eval_muted  # muted strings are not too troublesome with fingerstyle songs
        self.sugg_num = sugg_num

        self.chord = chord
        self.fings = set()
        self.fingering()

    def __repr__(self):
        sorted_fings = sorted([fng for fng in self.fings], key=self.eval_fng)
        string = ""

        for fng in sorted_fings[:5]:
            string += "{:<30}\t{:g}\n".format(str(fng), self.eval_fng(fng))

        return string

    def __str__(self):
        sorted_fings = sorted([fng for fng in self.fings], key=self.eval_fng)
        return "\n".join([self.pretty_print(fingering) for fingering in sorted_fings[:self.sugg_num]])

    def pretty_print(self, option: tuple) -> str:
        """
        A function for printing better comprehendable ASCII art of suggested chord fingering.

        Args:
            option: a tuple with fret positions in a fingering for each string

        Returns:
            ASCII art of suggested fingering option
        """
        pprint = ""

        min_pos = min(filter(lambda i: i > 0, option), default=0)
        max_pos = max(filter(lambda i: i > 0, option), default=0)
        req = len([i for i in option if i > 0])
        barre_pos = min_pos if (req > barre_req(option)) else -1

        # translate string positions into fret:[list of strings] pairs
        frets = defaultdict(list)
        for string_index in range(len(self.strings)):
            if option[string_index] != 0 and option[string_index] != -1:
                frets[option[string_index]].append(string_index)

        # print open/muted strings
        pprint += "   "
        for string_pos in option:
            if string_pos == -1:
                pprint += "X"
            elif string_pos == 0:
                pprint += "o"
            else:
                pprint += " "
        pprint += "\n"

        # prettier range
        diagram_range = range(min_pos, max_pos + 1)
        if max_pos < 4:
            diagram_range = range(1, max_pos + 1)
        if len(diagram_range) < 4:
            diagram_range = itertools.chain(diagram_range, range(max_pos + 1, max_pos + 5 - len(diagram_range)))

        # print all frets from min_pos
        for fret in diagram_range:
            pprint += "{:<3}".format(fret)
            stroke = False
            for string_index in range(len(self.strings)):
                if fret == barre_pos:
                    if stroke or (string_index in frets[fret]):
                        pprint += "="
                        stroke = True
                    else:
                        pprint += "|"
                    continue

                if string_index in frets[fret]:
                    pprint += "o"
                else:
                    pprint += "|"
            pprint += "\n"

        return pprint

    def fingering(self):
        """
        A function for finding fret positions for playing a chord specified.
        First it finds the position of the base note and then recursively finds fret positions for other strings.
        """
        if len(self.chord) > len(self.strings):
            return  # I ain't musically ready to approximate chords like this

        # generate all possible string position combinations
        first_note = self.chord.slash if self.chord.slash is not None else self.chord.root
        for first_string in range(len(self.strings) - len(self.chord) + 1):
            first_positions = string_positions(self.neck_length, self.strings[first_string], first_note)
            for position in first_positions:
                option = [-1 for _ in range(len(self.strings))]
                option[first_string] = position
                self.find_options(first_string + 1, option)

    def find_options(self, string_to_change: int, option: list):
        """
        A function for finding fret positions on a string specified for playing a chord specified.
        Args:
            string_to_change: an index of a string which fret positioning should be changed
            option: a list with fret positions in a yet incomplete fingering for each string
        """
        # base case
        if string_to_change >= len(self.strings):
            if self.valid(tuple(option)):
                self.fings.add(tuple(option))
            return

        for note in self.chord.notes:
            # try muted string and all string positions
            note_positions = [-1] + string_positions(self.neck_length, self.strings[string_to_change], note)
            for pos in note_positions:
                next_option = option.copy()
                next_option[string_to_change] = pos
                self.find_options(string_to_change + 1, next_option)

    def valid(self, option: tuple) -> bool:
        """
        A function for checking if proposed fingering option is valid.
        A valid fingering option should include all notes in the chord and
         should not require more than 4 fingers to be played.
        Args:
            option: a list with fret positions in a fingering for each string
        """
        # not all notes of the chord are being played
        used_notes = set()
        chord_notes = set(self.chord.notes)
        for string_index in range(len(self.strings)):
            if option[string_index] != -1:
                note = (self.strings[string_index] + option[string_index]) % 12
                used_notes.add(note)
        if len(chord_notes.difference(used_notes)) != 0:
            return False

        # requires more than 4 fingers
        req = len([i for i in option if i > 0])  # open and muted strings don't count
        if (req > 4) and barre_req(option) > 4:
            return False

        return True

    def eval_fng(self, option: tuple) -> int:
        """
        A function assigning the suggested fingering option a penalty based on its span,
        muted strings and its position on the fretboard.
        Args:
            option: a tuple with fret positions in a fingering for each string

        Returns:
            a number denoting a penalty of the suggested fingering option
        """
        min_pos = min(filter(lambda i: i > 0, option), default=0)
        max_pos = max(filter(lambda i: i > 0, option), default=0)
        penalty = 0

        # eval average span with quadratic bias
        span = max_pos - min_pos + 1
        penalty += (self.avg_span - span) ** 2

        # penalty for muted strings
        muted = 0
        if self.eval_muted:
            stroke = False
            for string_pos in option:
                if string_pos == -1:
                    if stroke:
                        muted += 1  # only eval muted strings in the middle of the stroke
                else:
                    stroke = True
            penalty += muted * 5

        # penalty for high positions
        mean_pos = sum(option) / len(option)
        penalty += mean_pos / 2

        # reduce penalty for barré chords
        req = len([i for i in option if i > 0])
        penalty -= (req - barre_req(option)) / 2

        return penalty
