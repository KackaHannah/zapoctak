# for internal use only!
# this shit scares me
def semitones(interval: str) -> int:
    """
    A function for translating interval symbols to their number of semitones.
    :param interval: symbolic name of an interval
    :return: number of semitones in a corresponding interval
    """
    if interval[0].isdigit():
        quality = ""
        number = int(interval)
    else:
        quality = interval[0]
        number = int(interval[1:])

    octaves = (number - 1) // 7
    base = number - (octaves * 7)
    semi = 0

    if base == 1 or base == 4 or base == 5:  # perfect intervals
        if base != 1:
            semi = (base * 2) - 3

        if quality == "d" or quality == "b":
            semi -= 1
        elif quality == "A" or quality == "#":
            semi += 1

    else:  # major-minor intervals
        if base < 4:
            semi = (base * 2) - 2
        else:
            semi = (base * 2) - 3

        if quality == "d":
            semi -= 2
        elif quality == "m" or quality == "b":
            semi -= 1
        elif quality == "A" or quality == "#":
            semi += 1

    return (octaves * 12) + semi


class Note:
    """
    Helper class for working with musical notes.

    While music theory clearly specifies when to use sharps and flats, this approach is too
    complicated for computer representation and is not that important most of the time.
    This codes enables corresponding sharps and flats to be used interchangeably.

    Notes are represented as integer values denoting the interval from note C to them.

    English key notation is being used (B is the symbol used for the seventh whole note in the C Dorian)

    Attributes:
        note_to_value: dictionary used for translating note symbols to integers
        value_to_note: dictionary used for translating integer values to notes
    """
    note_to_value = {
        'Bs': 0, 'C': 0, 'Cs': 1, 'Db': 1, 'D': 2, 'Ds': 3, 'Eb': 3,
        'E': 4, 'Fb': 4, 'Es': 5, 'F': 5, 'Fs': 6, 'Gb': 6, 'G': 7,
        'Gs': 8, 'Ab': 8, 'A': 9, 'As': 10, 'Bb': 10, 'B': 11, 'Cb': 11
    }
    # dictionary magic, swapping the keys for the values and vice versa
    value_to_note = {v: k for k, v in note_to_value.items()}

    @classmethod
    def val_by_name(cls, name: str) -> int:
        """
        Helper method to get integer value by name when using the '#' symbol for sharps.
        :param name:
        :return: integer value of note specified
        """
        if name[1:] == "#":
            name = name[0] + "s"

        try:
            val = cls.note_to_value[name]
        except KeyError:
            raise ValueError(f"Invalid string note '{name}'")
        return val
