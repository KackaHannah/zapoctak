# Achordeon

Achordeon is a simple command line tool for creating guitar chord fingerings.

## Installation

### pip

To install the application as a python package into your virtual environment, run the following command:

```bash
pip install git+ssh://git@gitlab.com/KackaHannah/achordeon
```

## Usage

To obtain the fingering of a specific chord, provide its name as the program's argument. The program’s output consists of the symbol of the chord parsed followed by 3 fingering suggestions by default. The number of suggestions shown and other parameters can be changed in options.

### Output

The tool’s output is an ASCII art of the suggested chord fingering. The ordering of the strings in the diagram is from top to bottom in terms of the order on the fingerboard.

The first line of the fingering contains information about open and muted strings. `X` stands for a muted string, `o` for an open string.

Each following line resembles one fret. It starts with the number of the fret, which is then followed by the string information. The `o` symbol indicates that the given string on that fret should be pressed, the `|` symbol indicates the opposite.

```
   XXo   
1  ||||||
2  |||o|o
3  ||||o|
4  ||||||
```

The `=` characters are shown in positions, where it’s recommended to use a barré.

```

1  ======
2  |||o||
3  |oo|||
4  ||||||
```

### Examples

Here are some example inputs and their corresponding outputs:

```bash
$ achordeon C/Edimmajadd9
C/Edimmajadd9
   o o   
2  |||||o
3  |o|o||
4  ||||o|
5  ||||||

   o o   
5  |||o||
6  |o|||o
7  ||||o|
8  ||||||

   o o   
11 |||o|o
12 ||||||
13 |o||o|
14 ||||||
```

```bash
$ achordeon F -n 1
F
         
1  ======
2  |||o||
3  |oo|||
4  ||||||
```

```bash
$ achordeon Dmi --strings E B G D A D
Dm
   XXo   
1  |||||o
2  |||o||
3  ||||o|
4  ||||||

   ooo   
1  |||||o
2  |||o||
3  ||||o|
4  ||||||

   oo    
1  |||||o
2  |||o||
3  ||o|o|
4  ||||||
```

## Chord syntax

Individual symbols of the chord descriptions should **not** be separated by spaces and the order of the symbols should be written as follows:

`root` `slash` `triad` `7th` `9th` `sus` `add`

- specification of the root note is mandatory, other modifications are optional
- if there is no triad specified, the major triad is used

### root

- `<note>` – natural
- `<note>#` – sharp
- `<note>b` – flat

### slash

- `/<note>` – replaces the bass note with the note after the slash
  - other notes and intervals of the chord are derived from the original root, not the slash

### triad

- the base of any chord consisting of a perfect unison, a third and a fifth

- no modification – major
- `m` – minor
  
- `dim` – diminished
  
- `+` – augmented

### 7th

- ads a 7th to a triad creating a seventh chord

- `7` – dominant
- `maj` – major

### 9th

- adds a 9th to a seventh chord creating a ninth chord

- if there is no 7th specified, it’s considered to be dominant

- `9` – major
- `b9` – minor
- `#9` – augmented

### sus

- `sus2` – suspended second
  
- `sus4` – suspended fourth


### add

- `add<interval>` – addition
  - `<interval>` – major
  - `b<interval> ` – minor
  - `#<interval>` – augmented